import xmlHttpRequest from 'xmlhttprequest';

// Skip imports that break unit tests, such as CSS.

function noop() {
    return;
}

require.extensions['.css'] = noop;
require.extensions['.svg'] = noop;

// Emulate XMLHttpRequest for code that assumes browser environment, for example
// Rx's Observable.ajax.
global.XMLHttpRequest = xmlHttpRequest.XMLHttpRequest;
