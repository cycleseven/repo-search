import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";
import "rxjs/add/observable/dom/ajax";
import "rxjs/add/observable/of";
import "rxjs/add/observable/merge";
import "rxjs/add/operator/debounceTime";
import "rxjs/add/operator/do";
import "rxjs/add/operator/distinctUntilChanged";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/map";
import "rxjs/add/operator/switch";
import "rxjs/add/operator/take";

import api from "../common/api";

const DEBOUNCE_TIME_MS = 400;

/**
 * Instantiate a search client.
 *
 * Example usage :
 * const searchClient = createSearchClient({ next: results => this.setState({ results }) });
 *
 * // `next` callback will be called when the results for this query resolve
 * searchClient.updateQuery("some search query");
 *
 * @param {?{next: ?function, error: ?function}}
 *        Callbacks fired when search results resolve successfully
 *
 * @return {{ updateQuery: function }}
 */
function createSearchClient({ next, error } = {}) {
  const queryStream = new Subject();

  const apiRequestStream = queryStream
    .filter(query => query !== "")
    .debounceTime(DEBOUNCE_TIME_MS)
    .map(query => Observable.ajax(api.searchRepositories(query)));

  const clearSearchStream = queryStream
    .filter(query => query === "")
    .map(() => Observable.of({ response: { items: [] } }));

  const resultsStream = Observable.merge(apiRequestStream, clearSearchStream)
    .switch()
    .map(apiResponse => apiResponse.response.items);

  resultsStream.subscribe({ next, error });

  function updateQuery(query) {
    queryStream.next(query);
  }

  return { updateQuery, resultsStream };
}

export { DEBOUNCE_TIME_MS, createSearchClient };
