import React from "react";
import styles from "./styles.module.css";
import { ReactComponent as SearchIcon } from "../../../common/icons/searchIcon.svg";

function SearchInput({ onChange }) {
  return (
    <div className={styles.root}>
      <input
        autoCapitalize="none"
        autoComplete="off"
        autoFocus
        className={styles.input}
        onChange={event => onChange(event.target.value)}
        placeholder="Search"
        spellCheck="false"
        type="text"
      />
      <div className={styles.icon}>
        <SearchIcon alt="search" />
      </div>
    </div>
  );
}

export default SearchInput;
