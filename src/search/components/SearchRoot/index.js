import React from "react";

import QueryInput from "../QueryInput";
import Results from "../Results";
import styles from "./styles.module.css";
import { createSearchClient } from "../../searchClient";

function SearchRoot() {
  const [results, setResults] = React.useState([]);
  const searchClient = React.useRef();
  React.useEffect(() => {
    searchClient.current = createSearchClient({ next: setResults });
  }, []);

  return (
    <div className={styles.root}>
      <QueryInput onChange={query => searchClient.current.updateQuery(query)} />
      <Results results={results} />
    </div>
  );
}

export default SearchRoot;
