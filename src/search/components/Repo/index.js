import React from "react";
import styles from "./styles.module.css";

function Repo(props) {
  return (
    <div className={styles.root}>
      <a className={styles.name} href={props.repo.html_url}>
        {props.repo.name}
      </a>
    </div>
  );
}

export default Repo;
