import React from "react";

import Repo from "../Repo";
import styles from "./styles.module.css";

function Results({ results }) {
  return (
    <ul className={styles.root}>
      <li>
        {results.map((result, index) => (
          <Repo key={index} repo={result} />
        ))}
      </li>
    </ul>
  );
}

export default Results;
