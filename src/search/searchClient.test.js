import { assert } from "chai";
import sinon from "sinon";

import api from "../common/api";
import { createSearchClient, DEBOUNCE_TIME_MS } from "./searchClient";

describe("Search client", function() {
  let clock;
  let server;

  beforeEach(function() {
    // It slows down the test suite if devs have to wait on timeouts etc. in real time.
    // Use virtual time instead.
    clock = sinon.useFakeTimers();
    server = sinon.fakeServer.create({
      respondImmediately: true
    });

    // Github responds with 422 if the query parameter is missing.
    // Emulate this behaviour so that tests will fail realistically if the code fails to handle
    // empty queries properly.
    server.respondWith("GET", api.searchRepositories(""), [422, {}, ""]);
  });

  afterEach(function() {
    clock.restore();
    server.restore();
  });

  /*
   * Github responds with a list called `items`. That's what these tests are interested in, so
   * just include that part in the fake response.
   */
  function expectQuery(query) {
    const fakeResponse = JSON.stringify({ items: ["woo"] });
    return server.respondWith("GET", api.searchRepositories(query), [
      200,
      {},
      fakeResponse
    ]);
  }

  describe("updateQuery method", () => {
    it("should make a debounced request to the search API when query changes", done => {
      // Tell sinon which path + query params we expect. This means this test implictly verifies
      // that the search client forms the Github API URL correctly.
      expectQuery("fake query");

      // Subscribe to the stream of API requests. The fact that Rx's `.subscribe()` API is used
      // directly means this unit test depends on Rx being used in the implementation. This is
      // probably a bad thing and could be improved by hiding the Rx API in the searchClient
      // interface.
      const searchClient = createSearchClient({
        next: () => {
          // Include a message to make assertion failures clearer.
          assert(
            server.requests.length === 1,
            "Request should have happened, but it didn't."
          );

          // Call the done() callback to signal to Mocha that the async test is complete.
          done();
        },

        // Also call the done() callback if the Ajax stream throws an error. This allows the
        // test to report a more useful error.
        error: done
      });

      // Run that sick code.
      searchClient.updateQuery("fake query");

      assert(
        server.requests.length === 0,
        "Request should not be done. Debounce time has not passed yet."
      );

      // Increment the fake time, so that the debounce period passes.
      clock.tick(DEBOUNCE_TIME_MS);
    });

    it("should clear search when query becomes empty", done => {
      expectQuery("fake query");

      let results = [];
      const searchClient = createSearchClient();
      searchClient.resultsStream.take(2).subscribe({
        next: newResults => {
          results = newResults;
        },
        complete: () => {
          assert(results.length === 0, "Result should be empty list!");
          done();
        },
        error: done
      });

      searchClient.updateQuery("fake query");
      clock.tick(DEBOUNCE_TIME_MS);
      searchClient.updateQuery("");
      clock.tick(DEBOUNCE_TIME_MS);
    });
  });
});
