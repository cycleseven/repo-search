# Fonts

At the time of writing, most of the fonts aren't shipped in the bundle. The full set of Lato fonts
are included in the repo anyway, to make it easier to add new @font-face declarations later.
