const API_ROOT = "https://api.github.com";
const SEARCH_REPOSITORIES_PATH = "/search/repositories";

function searchRepositories(query) {
  return `${API_ROOT}${SEARCH_REPOSITORIES_PATH}?q=${query}`;
}

export default {
  API_ROOT,
  SEARCH_REPOSITORIES_PATH,
  searchRepositories
};
