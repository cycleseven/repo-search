import React from "react";

import styles from "./styles.module.css";

function Icon({ alt, src }) {
  return <img alt={alt} className={styles.icon} src={src} />;
}

export default Icon;
